package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	public CreateLeadPage enterCname() {
		clearAndType(eleCompanyName, "TestLeaf");
		return this; 
	}
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	public CreateLeadPage enterFname() {
		clearAndType(eleFirstName, "Gayatri");
		return this; 
	}
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	public CreateLeadPage enterLname() {
		clearAndType(eleLastName, "K");
		return this; 
	}
	@FindBy(how = How.CLASS_NAME,using="smallSubmit") WebElement eleCreateLeadButton;
	public CreateLeadPage clickCreateLeadButton() {
		click(eleCreateLeadButton);
		return this; 
	}


}
















